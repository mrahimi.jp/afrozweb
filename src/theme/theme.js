import { createTheme, responsiveFontSizes } from '@material-ui/core/styles'
import palette from './palette'

const theme = createTheme({
  palette,
  typography: {
    allVariants: {
      textTransform: 'lowercase'
    },
    h1: {
      fontSize: 62,
      fontFamily: "Playfair Display",
      color: palette.sliderHeader,
      fontWeight: 'bold',
      textTransform: 'capitalize'
    },
    h2: {
      fontSize: 26,
      fontFamily: "Playfair Display",
      color: palette.quoteColor,
      lineHeight: 1.462,
      textAlign: 'center',
    },
    h3: {
      fontSize: 42,
      fontFamily: "Playfair Display",
      color: palette.title,
      fontWeight: 'bold',
      lineHeight: 1.8,
      textAlign: 'center',
    },
    h4: {
      fontSize: 26,
      fontFamily: "Playfair Display",
      color: 'rgb(51, 51, 51)',
      lineHeight: 1.077,
      textAlign: 'left',
    },
    h5: {
      fontSize: 12,
      fontFamily: "Poppins",
      color: 'rgb(122, 122, 122)',
      textTransform: 'uppercase',
      lineHeight: 1.2,
      textAlign: 'left',
    },
    h6: {
      fontSize: 9,
      fontFamily: "League Spartan",
      color: 'rgba(93, 168, 239, 0.902)',
      fontWeight: 'bold',
      textTransform: 'uppercase',
      textAlign: 'left',
      position: 'absolute',
      letterSpacing: 3
    },
    body1: {
      fontSize: 9,
      fontFamily: "League Spartan",
      color: palette.menuItem,
      fontWeight: 'bold',
      textTransform: 'uppercase',
      textAlign: 'left',
      letterSpacing: 5
    },
    body2: {
      fontSize: 9,
      fontFamily: "League Spartan",
      color: 'rgba(51, 51, 51, 0.902)',
      fontWeight: 'bold',
      textTransform: 'uppercase',
      lineHeight: 7.333,
      textAlign: 'center',
    },
    subtitle1: {
      fontSize: 14,
      fontFamily: "Poppins",
      color: palette.mediaCaption,
      lineHeight: 1.714,
      textAlign: 'left',
    },
    subtitle2: {
      fontSize: 9,
      fontFamily: "League Spartan",
      color: palette.rightCol,
      fontWeight: 'bold',
      textTransform: 'uppercase',
      textAlign: 'right',
      writingMode: 'vertical-rl',
      transform: 'rotate(180deg)',
      letterSpacing: 3
    },
    caption: {
      fontSize: 14,
      fontFamily: "Poppins",
      color: palette.sliderCaption,
      lineHeight: 1.714,
      textAlign: 'left',
    },
    button: {
      fontSize: 9,
      fontFamily: "League Spartan",
      color: palette.mediaButton,
      fontWeight: 'bold',
      textTransform: 'uppercase',
      textAlign: 'left',
      letterSpacing: 3
    }

  }
})

export default responsiveFontSizes(theme)
