/* eslint-disable import/no-anonymous-default-export */
export default {
  lineColor: 'rgba(88, 88, 88, 0.24)',
  menuItem: 'rgba(11, 12, 17, 0.902)',
  rightCol: 'rgba(11, 12, 17, 0.2)',
  sliderHeader: 'rgb(11, 12, 17)',
  sliderCaption: 'rgb(143, 143, 143)',
  quoteColor: 'rgba(51, 51, 51, 0.902)',
  mediaCaption: 'rgba(97, 97, 97, 0.651)',
  mediaButton: 'rgb(51, 51, 51)',
  title: 'rgb(51, 51, 51)',
  productBg: 'rgb(232, 232, 232)'
};
