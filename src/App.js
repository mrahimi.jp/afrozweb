import { ThemeProvider } from '@material-ui/styles';
import CssBaseline from '@mui/material/CssBaseline'
import theme from './theme/theme'
import Home from './screens'

function App() {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Home />
    </ThemeProvider>
  );
}

export default App;
