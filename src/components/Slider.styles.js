import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    root: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'right',
    },
    image: {
        // boxShadow: '-15.898px 25.441px 65px 0px rgba(11, 12, 17, 0.2)',
        width: '90%',
        // height: 572,
        top: 0,
        right: 0,
    },
    textContainer: {
        position: 'absolute',
        left: 0,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'left'
    },
    title: {
        padding: '20px 0'
    },
    caption: {
        width: 400
    }
}))
