import React from 'react'
import useStyles from './MediaContent.styles'
import Photo from '../assets/images/mediaPhoto.png'
import Video from '../assets/images/mediaVideo.png'
import { Typography } from '@material-ui/core'
import { Box } from '@mui/system'
import UnderlinedButton from './UnderlinedButton'

export default function MediaContent() {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Box className={classes.leftContent}>
                <img src={Photo} alt='' />
                <div className={classes.captionDiv}>
                    <Typography className={classes.caption}
                        variant='subtitle1'>
                        By better understanding the various aspects of surfing, you will improve
                        faster and have more fun in the water.
                    </Typography>
                    <UnderlinedButton text='read more'/>
                </div>
            </Box>
            <Box className={classes.rightContent}>
                <img className={classes.video} src={Video} alt='' />
                <span className={classes.playCircle}>
                    <span className={classes.play} />
                </span>
            </Box>
        </div>
    )
}
