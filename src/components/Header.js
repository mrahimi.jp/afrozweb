import React from 'react'
import useStyles from './Header.styles'
import logoIcon from '../assets/images/logo.png'
import { Button, Grid, Typography } from '@material-ui/core'

export default function Header() {
    const classes = useStyles()

    return (
        <Grid className={classes.root}>
            <img classes={classes.logo} src={logoIcon} alt='afroz web'/>
            <Grid className={classes.menu}>
                <Button className={classes.menuItem} variant='text'>
                    <Typography variant='body1'>stories</Typography>
                </Button>
                <Button className={classes.menuItem} variant='text'>
                    <Typography variant='body1'>events</Typography>
                </Button>
                <Button className={classes.menuItem} variant='text'>
                    <Typography variant='body1'>places</Typography>
                </Button>
                <Button className={classes.menuItem} variant='text'>
                    <Typography variant='body1'>boards</Typography>
                </Button>
            </Grid>
        </Grid>
    )
}
