import { Typography, Divider } from '@material-ui/core'
import React from 'react'
import useStyles from './Quote.styles'

export default function Quote() {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Typography variant='h2'>Surfing is the most blissful experience you can have
                on this planet, a taste of heaven.</Typography>
            <Typography variant='body2'>John McCarthy</Typography>
            <Divider className={classes.line} orientation="vertical" flexItem />
        </div>
    )
}
