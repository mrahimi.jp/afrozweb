import { Typography } from '@material-ui/core'
import React from 'react'
import useStyles from './Shop.styles'
import Carousel from 'react-material-ui-carousel'
import board1Image from '../assets/images/board1.png'
import board2Image from '../assets/images/board2.png'
import board3Image from '../assets/images/board3.png'
import Product from './Product'
import UnderlinedButton from './UnderlinedButton'
import LeftIcon from '../assets/images/Left.png'
import RightIcon from '../assets/images/Right.png'

const surfboards = [
    {
        item1: { type: 'Funboards', title: 'Agency GROM', price: '$670', image: board1Image },
        item2: { type: 'Surfboards', title: 'Emery NEM XF', price: '$950', image: board2Image },
        item3: { type: 'Funboards', title: 'Chilli Rare Bird', price: '$890', image: board3Image },
    },
    {
        item1: { type: 'Funboards', title: 'Agency GROM', price: '$670', image: board1Image },
        item2: { type: 'Surfboards', title: 'Emery NEM XF', price: '$950', image: board2Image },
        item3: { type: 'Funboards', title: 'Chilli Rare Bird', price: '$890', image: board3Image },
    },
    {
        item1: { type: 'Funboards', title: 'Agency GROM', price: '$670', image: board1Image },
        item2: { type: 'Surfboards', title: 'Emery NEM XF', price: '$950', image: board2Image },
        item3: { type: 'Funboards', title: 'Chilli Rare Bird', price: '$890', image: board3Image },
    },
]

export default function Shop() {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Typography variant='h6'>shop</Typography>
            <Typography variant='h3'>Surfboards</Typography>
            <Carousel className={classes.slider}
                indicators={false}
                navButtonsAlwaysVisible={true}
                NextIcon={<img src={RightIcon} alt='' />}
                PrevIcon={<img src={LeftIcon} alt='' />}>
                {surfboards.map((item, i) => <Product key={i} items={item} />)}
            </Carousel>
            <UnderlinedButton text='show all' />
        </div>
    )
}
