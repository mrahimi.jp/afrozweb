import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Typography } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'realtive',
        display: 'flex'
    },
    title: {
        fontSize: 42,
        fontFamily: "Playfair Display",
        color: 'rgb(51, 51, 51)',
        fontWeight: 'bold',
        lineHeight: 2.3,
        textAlign: 'left',
        textTransform: 'capitalize',
        position: 'relative',
        "&::before": {
            content: "''",
            backgroundColor: 'rgb(93, 168, 239)',
            position: 'absolute',
            top: 90,
            width: 23,
            height: 2,
        }
    },
    subtitle: {
        fontSize: 130,
        fontFamily: "League Spartan",
        color: 'rgb(249, 249, 249)',
        fontWeight: 'bold',
        lineHeight: 0.508,
        position: 'absolute'
    }
}))

export default function ArticleTitle({ title, subtitle }) {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <span className={classes.subtitle}>{subtitle}</span>
            <Typography className={classes.title}>{title}</Typography>
        </div>
    )
}
