import { Typography, Divider } from '@material-ui/core'
import React from 'react'
import ArticleTitle from './ArticleTitle'
import useStyles from './Article2.styles'
import UnderlinedButton from './UnderlinedButton'
import article2Image from '../assets/images/article2.png'

export default function Article() {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Typography variant='subtitle2'
                className={classes.verticalText}>
                Surf Camps
            </Typography>
            <img className={classes.image} src={article2Image} alt='' />
            <div className={classes.textDiv}>
                <ArticleTitle title='Point Break' subtitle='02' />
                <Typography className={classes.caption}
                    variant='subtitle1'>
                    By better understanding the various aspects of By better understanding the various aspects of surfing, By better understanding the various aspects of surfing, you will improve faster and have more fun in the water.
                </Typography>
                <UnderlinedButton text='read more' />
            </div>
        </div>
    )
}
