import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    root: {
        width: '45%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 80,
        position: 'relative'
    },
    line: {
        backgroundColor: 'rgb(93, 168, 239)',
        position: 'relative',
        left: '50%',
        width: 2,
        height: 100,
        marginBottom: 50
    },
    caption: {
        color: 'rgba(97, 97, 97, 0.651)',
        textAlign: 'center'
    },
    inputDiv: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: 590,
        height: 50,
        backgroundColor: 'rgb(245, 245, 245)',
        boxShadow: 'inset 0px 1px 2px 0px rgba(0, 0, 0, 0.03)',
        paddingLeft: 20,
    },
    input: {
        fontSize: 9,
        fontFamily: "League Spartan !important",
        color: 'rgba(51, 51, 51, 0.902) !important',
        fontWeight: 'bold !important',
        textTransform: 'uppercase',
        letterSpacing: '3 !important'
    },
    inputButton: {
        width: 50,
        height: 50,
        backgroundColor: 'rgb(93, 168, 239) !important',
        borderRadius: '0 !important'
    },
    icon: {
        color: 'white',
        fontSize: 30
    }
}))
