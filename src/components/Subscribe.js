import { Typography, Divider } from '@material-ui/core'
import InputBase from '@mui/material/InputBase';
import IconButton from '@mui/material/IconButton';
import ChevronRight from '@material-ui/icons/ChevronRight'
import React from 'react'
import useStyles from './Subscribe.styles'

export default function Subscribe() {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Divider className={classes.line} orientation="vertical" flexItem />
            <Typography variant='h3'>Join the Club</Typography>
            <Typography className={classes.caption} variant='caption'>
                By better understanding the various aspects of surfing, you will improve faster
                and have more fun in the water.
            </Typography>
            <div className={classes.inputDiv} >
                <InputBase classes={{input: classes.input}} placeholder='your e-mail' />
                <IconButton className={classes.inputButton} >
                    <ChevronRight className={classes.icon} />
                </IconButton>
            </div>
        </div>
    )
}
