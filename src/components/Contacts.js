import { Typography, Button } from '@material-ui/core'
import React from 'react'
import useStyles from './Contacts.styles'
import contactImage from '../assets/images/contact.png'

export default function Contacts() {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <img className={classes.image} src={contactImage} alt='' />
            <div className={classes.textDiv}>
                <Typography className={classes.header}>
                    Our Camp
                </Typography>
                <Typography className={classes.caption}
                    variant='subtitle1'>
                    CA 91932, USA<br />
                    Imperial Beach<br />
                    560 Silver Strand Blvd
                </Typography>
                <Button className={classes.button}>
                    <Typography className={classes.buttonText}
                        variant='button'>
                        Get in Touch
                    </Typography>
                </Button>
            </div>
        </div>
    )
}
