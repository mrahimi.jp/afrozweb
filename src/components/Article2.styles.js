import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 150,
        marginLeft: 70
    },
    textDiv: {
        width: '31%',
        paddingLeft: 50,
        marginTop: 50,
        [theme.breakpoints.down('md')]: {
            width: '70%',
        }
    },
    caption: {
        marginTop: 20,
        marginBottom: 40
    },
    image: {
        height: 439,
        [theme.breakpoints.down('md')]: {
            width: '30%',
            height: 'fit-content'
        }
    },
    verticalText: {
        marginLeft: 20
    }
}))
