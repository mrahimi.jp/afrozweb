import React from 'react'
import useStyles from './Footer.styles'
import LogoIcon from '../assets/images/logo.png'
import { Button, Grid, Typography } from '@material-ui/core'
import VimeoIcon from '../assets/images/vimeo.png'
import TumblrIcon from '../assets/images/tumblr.png'
import TwitterIcon from '../assets/images/twitter.png'

export default function Footer() {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <img className={classes.logo} src={LogoIcon} alt='' />
            <Grid className={classes.menu}>
                <Button className={classes.menuItem} variant='text'>
                    <Typography variant='body1'>stories</Typography>
                </Button>
                <Button className={classes.menuItem} variant='text'>
                    <Typography variant='body1'>events</Typography>
                </Button>
                <Button className={classes.menuItem} variant='text'>
                    <Typography variant='body1'>places</Typography>
                </Button>
                <Button className={classes.menuItem} variant='text'>
                    <Typography variant='body1'>boards</Typography>
                </Button>
            </Grid>
            <div className={classes.iconContainer}>
                <img className={classes.icon} src={TumblrIcon} alt='' />
                <img className={classes.icon} src={TwitterIcon} alt='' />
                <img className={classes.icon} src={VimeoIcon} alt='' />
            </div>
        </div>
    )
}
