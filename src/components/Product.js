import React from 'react'
import useStyles from './Product.styles'
import { Button, Typography } from '@material-ui/core'


export default function ProductGroup({ items }) {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Product item={items.item1} />
            <Product item={items.item2} />
            <Product item={items.item3} />
        </div>
    )
}

function Product({ item }) {
    const classes = useStyles()

    return (
        <div className={classes.product}>
            <div className={classes.imageContainer}>
                <div className={classes.imageBg} />
                <img className={classes.image} src={item.image} alt='' />
            </div>
            <div className={classes.textContainer}>
                <Typography variant='h5'>{item.type}</Typography>
                <Typography variant='h4'>{item.title}</Typography>
                <div className={classes.priceContainer}>
                    <Typography className={classes.price}>{item.price}</Typography>
                    <Button className={classes.button}>
                        <Typography variant='button'>Buy</Typography>
                    </Button>
                </div>
            </div>
        </div>
    )
}


