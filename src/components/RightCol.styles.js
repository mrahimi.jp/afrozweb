import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    root: {
        height: '87vh',
        position: 'absolute',
        right: '5%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        paddingTop: 38,
        paddingBottom: 10,
    },
    colText: {
        position: 'relative'
    },
    icon: {
        height: 17,
        width: 'fit-content'
    },
    iconContainer: {
        height: 100,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
}))
