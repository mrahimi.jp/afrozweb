import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 100,
        width: '110%'
    },
    textDiv: {
        width: '50%',
        paddingLeft: 50,
        marginTop: 80
    },
    caption: {
        marginTop: 50,
        marginBottom: 40,
        paddingLeft: 50,
        fontSize: 32,
        fontFamily: "Playfair Display",
        color: 'rgb(51, 51, 51)',
        fontWeight: 'bold',
        lineHeight: 1.438,
        textAlign: 'left',
    },
    image: {
        width: '50%'
    },
    header: {
        fontSize: 9,
        fontFamily: "League Spartan",
        color: theme.palette.rightCol,
        fontWeight: 'bold',
        textTransform: 'uppercase',
        textAlign: 'left',
        letterSpacing: 3
    },
    button: {
        marginTop: 30,
        paddingLeft: 0,
        paddingRight: 0,
        borderRadius: 0,
        borderBottom: '1px solid rgb(93, 168, 239)'
    },
    buttonText: {
        color: 'rgb(93, 168, 239)'
    }
}))
