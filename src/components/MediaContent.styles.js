import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    root: {
        width: '91%',
        marginRight: '-2%',
        height: '100%',
        marginTop: 200,
        position: 'relative'
    },
    leftContent: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '50%',
        [theme.breakpoints.down('md')]: {
            width: '100%'
        }
    },
    captionDiv: {
        width: '60%',
        marginTop: 40,
    },
    rightContent: {
        position: 'absolute',
        right: 0,
        top: 30,
        [theme.breakpoints.down('md')]: {
            top: 'unset'
        }

    },
    video: {
        // backgroundColor: 'rgb(93, 168, 239)',
        boxShadow: '-2.121px - 2.121px 35px 0px rgba(0, 0, 0, 0.19)',
        width: '100%',
        // height: 549,
    },
    playCircle: {
        borderRadius: '50%',
        backgroundColor: 'rgb(93, 168, 239)',
        boxShadow: '-2.121px -2.121px 35px 0px rgba(0, 0, 0, 0.13)',
        position: 'absolute',
        left: '45%',
        top: '45%',
        width: 68,
        height: 68,
    },
    play: {
        position: 'absolute',
        left: '42%',
        top: '33%',
        width: 0,
        height: 0,
        borderTop: '14px solid transparent',
        borderBottom: '14px solid transparent',
        borderLeft: '17px solid white',
    }
}))
