import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
    },
    product: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'relative',
        width: 400,
        height: '100%',
    },
    imageContainer: {
        position: 'relative',
        width: '100%',
        height: 800,
        display: 'flex',
        alignItems: 'center'
    },
    imageBg: {
        backgroundColor: theme.palette.productBg,
        width: 250,
        height: 320,
        bottom: 0
    },
    image: {
        width: 247,
        height: 549,
        position: 'absolute',
        top: 0
    },
    textContainer: {
        position: 'absolute',
        marginLeft: '50%'
    },
    price: {
        fontSize: 20,
        fontFamily: "Poppins",
        color: 'rgb(206, 96, 168)',
        lineHeight: 1.2,
        textAlign: 'left',
    },
    button: {
        paddingLeft: 0,
        paddingRight: 0,
        display: 'flex',
        justifyContent: 'left'
    },
    priceContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginTop: 50
    }
}))
