import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Button, Typography } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: 30,
        paddingLeft: 0,
        paddingRight: 0,
        borderRadius: 0,
        borderBottom: '1px solid rgb(93, 168, 239)'
    }
}))

export default function UnderlinedButton({ text }) {
    const classes = useStyles();
    return (
        <Button className={classes.root}>
            <Typography variant='button'>{text}</Typography>
        </Button>
    )
}
