import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    root: {
        width: 610,
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 150,
        position: 'relative'
    },
    logo: {
        width: 16,
        height: 44,

    },
    menu: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 50
    },
    menuItem: {
        width: 100
    },
    icon: {
        height: 17,
        width: 'fit-content'
    },
    iconContainer: {
        width: 150,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 40
    }
}))
