import { Typography } from '@material-ui/core'
import React from 'react'
import useStyles from './RightCol.styles'
import VimeoIcon from '../assets/images/vimeo.png'
import TumblrIcon from '../assets/images/tumblr.png'
import TwitterIcon from '../assets/images/twitter.png'

export default function RightCol() {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Typography variant='subtitle2'
                className={classes.colText}>
                first surfing magazine
            </Typography>
            <div className={classes.iconContainer}>
                <img className={classes.icon} src={TumblrIcon} alt='' />
                <img className={classes.icon} src={TwitterIcon} alt='' />
                <img className={classes.icon} src={VimeoIcon} alt='' />
            </div>
        </div>
    )
}
