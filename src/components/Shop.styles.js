import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    root: {
        width: '100%',
        height: '100%',
        marginTop: 180,
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        [theme.breakpoints.down('md')]: {
            marginTop: '75%'
        }
    },
    slider: {
        width: '100%',
        height: 580,
    }
}))
