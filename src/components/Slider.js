import React from 'react'
import useStyles from './Slider.styles'
import Carousel from 'react-material-ui-carousel'
import { Typography } from '@material-ui/core'


export default function Slider({ item }) {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <div className={classes.textContainer}>
                <Typography variant='h6'>your</Typography>
                <Typography className={classes.title}
                    variant='h1'>
                    {item.title}
                </Typography>
                <Typography className={classes.caption}
                    variant='caption'>
                    {item.caption}
                </Typography>
            </div>
            <img className={classes.image} src={item.image} alt='' />
        </div>
    )
}
