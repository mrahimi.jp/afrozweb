import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    root: {
        width: '45%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 100,
        position: 'relative'
    },
    line: {
        backgroundColor: 'rgb(93, 168, 239)',
        position: 'absolute',
        left: '50%',
        top: 170,
        width: 2,
        height: 100,
    }
}))
