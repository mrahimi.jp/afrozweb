import { Typography, Divider } from '@material-ui/core'
import React from 'react'
import ArticleTitle from './ArticleTitle'
import useStyles from './Article1.styles'
import UnderlinedButton from './UnderlinedButton'
import article1Image from '../assets/images/article1.png'

export default function Article() {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <div className={classes.textDiv}>
                <ArticleTitle title='Surf Training' subtitle='01' />
                <Typography className={classes.caption}
                    variant='subtitle1'>
                    By better understanding the various aspects of surfing, By better understanding the various aspects of surfing, you will improve faster and have more fun in the water.</Typography>
                <UnderlinedButton text='read more' />
            </div>
            <img className={classes.image} src={article1Image} alt='' />
            <Typography variant='subtitle2'
                className={classes.verticalText}>
                Surf Camps
            </Typography>
        </div>
    )
}
