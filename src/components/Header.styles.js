import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexDirection: 'row',
        paddingTop: 38,
        paddingBottom: 38,
        height: 120,
        marginLeft: '5.6%'
    },
    logo: {
        width: 16,
        height: 44,
        left: 0
    },
    menu: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: 190
    },
    menuItem: {
        width: 100
    }
}))
