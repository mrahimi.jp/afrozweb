import { makeStyles } from '@material-ui/core/styles'

export default makeStyles((theme) => ({
    root: {
        height: 5251,
        margin: '0 5%',
        paddingBottom: 500,
        // paddingLeft: '5%'
    },
    bg: {
        // width: '80%',
        display: 'flex',
        flexDirection: 'column',
        // margin: '0 5.6%'
    },
    firstFade: {
        position: 'absolute',
        top: 1572,
        width: '100%',
        height: 'auto',
        zIndex: 3,
    },
    secondFade: {
        position: 'absolute',
        width: '100%',
        top: 3525,
        widht: '100%',
        height: 'auto'
    },
    lineContainer: {
        display: 'flex',
        flexDirection: 'row',
    },
    line1: {
        backgroundColor: theme.palette.lineColor,
        position: 'absolute',
        left: '10%',
        width: 1,
        height: 5251,
        zIndex: -1
    },
    line2: {
        backgroundColor: theme.palette.lineColor,
        position: 'absolute',
        left: '30%',
        width: 1,
        height: 5251,
        zIndex: -1
    },
    line3: {
        backgroundColor: theme.palette.lineColor,
        position: 'absolute',
        left: '50%',
        width: 1,
        height: 5251,
        zIndex: -1
    },
    line4: {
        backgroundColor: theme.palette.lineColor,
        position: 'absolute',
        left: '70%',
        width: 1,
        height: 5251,
        zIndex: -1
    },
    line5: {
        backgroundColor: theme.palette.lineColor,
        position: 'absolute',
        left: '90%',
        width: 1,
        height: 5251,
        zIndex: -1
    },
    slider: {
        width: '91%',
        marginRight: '-3%'
    },
    content: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    }
}))
