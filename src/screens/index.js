import React from 'react'
import useStyles from './index.styles'
import Fade1Image from '../assets/images/fade1.png'
import Fade2Image from '../assets/images/fade2.png'
import Header from '../components/Header';
import RightCol from '../components/RightCol';
import Carousel from 'react-material-ui-carousel'
import sliderImage from '../assets/images/sliderImage.png'
import Slider from '../components/Slider';
import Quote from '../components/Quote';
import MediaContent from '../components/MediaContent';
import Shop from '../components/Shop';
import Article1 from '../components/Article1';
import Article2 from '../components/Article2';
import Subscribe from '../components/Subscribe';
import Contacts from '../components/Contacts';
import Footer from '../components/Footer';

var items = [
    {
        id: 1,
        title: 'Beautiful Escape',
        caption: 'one of the greatest things about the sport of surfing is that you need only three things: your body, a surfboard, and a wave.',
        image: sliderImage
    },
    {
        id: 2,
        title: 'Beautiful Escape',
        caption: 'one of the greatest things about the sport of surfing is that you need only three things: your body, a surfboard, and a wave.',
        image: sliderImage
    },

]

export default function Home() {
    const classes = useStyles();


    return (
        <div className={classes.root}>
            <RightCol />
            <div className={classes.bg}>
                <div className={classes.lineContainer}>
                    <span className={classes.line1} />
                    <span className={classes.line2} />
                    <span className={classes.line3} />
                    <span className={classes.line4} />
                    <span className={classes.line5} />
                </div>
                <Header />
                <div className={classes.content}>
                    <Carousel className={classes.slider}>
                        {items.map((item, i) => <Slider key={i} item={item} />)}
                    </Carousel>
                    <Quote />
                    <MediaContent />
                    <Shop />
                    <img className={classes.firstFade} src={Fade1Image} alt='afroz web' />
                    <Article1 />
                    <Article2 />
                    <img className={classes.secondFade} src={Fade2Image} alt='afroz web' />
                    <Subscribe />
                    <Contacts />
                    <Footer />
                </div>

            </div>
        </div>
    )
}
